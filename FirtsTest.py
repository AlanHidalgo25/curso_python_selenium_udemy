from typing import KeysView
import unittest
from selenium import webdriver
from time import sleep

class FirtsTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=
        r"C:/Users/alan8/Documents/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(15)
        driver.maximize_window()
        driver.get('https://demoqa.com/text-box')

    def test_text_box(self):
        driver = self.driver
        name = driver.find_element_by_id('userName')
        name.click()
        name.send_keys('Alan Hidalgo')
        sleep(1)

        emial = driver.find_element_by_id('userEmail')
        emial.send_keys('alan@gmail.com')
        sleep(1)

        address = driver.find_element_by_id('currentAddress')
        address.send_keys('Santiago de chile')
        sleep(1)

        address_2 = driver.find_element_by_id('permanentAddress')
        address_2.send_keys('portugal 755')
        sleep(1)

        driver.execute_script('window.scrollTo(0,300)')

        button_submit = driver.find_element_by_id('submit')
        button_submit.click()
        sleep(1)

        output = []

        #imprimo por pantalla los valores llenados y
        #enviados por el formulario para validar
        for i in range(4):
            form_output = driver.find_element_by_xpath(f'/html/body/div[2]/div/div/div[2]/div[2]/div[1]/form/div[6]/div/p[{i+1}]')
            output.append(form_output.text)
            
        print(output)

    def tearDown(self):
            self.driver.implicitly_wait(5)
            self.driver.close()

if __name__ == "__main__":
	unittest.main(verbosity = 2)