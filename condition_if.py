import unittest
from pyunitreport import HTMLTestRunner
from selenium import webdriver
#para poder elegir varias opciones en un dropdown se importa
from selenium.webdriver.support.ui import Select
from time import sleep

class ConditionIf(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=
        r"C:/Users/alan8/Documents/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(30)
        driver.maximize_window()
        driver.get('https://demoqa.com/')

    def test_condition_if(self):
        driver = self.driver
        url = "https://demoqa.com/"
        title = driver.title
        image = driver.find_element_by_xpath("//header/a[1]/img[1]")

        try:
            if(title == "ToolsQA" and image.is_displayed()==True):
                print(f"El titulo es correcto {title} y se muestra la imagen")
            else:
                print('el titulo o la imagen tienen problemas')
        except Exception as e:
            print(e)
            print("No se encontro nada")
        

    def tearDown(self):
        self.driver.implicitly_wait(5)
        self.driver.close()

if __name__ == "__main__":
	unittest.main(verbosity = 2)