import unittest
from pyunitreport import HTMLTestRunner
from selenium import webdriver
#para poder elegir varias opciones en un dropdown se importa
from selenium.webdriver.support.ui import Select
from time import sleep
#importamos de la siguiente manera
#de la carpeta Funciones selecciona el archivo que se llama Funciones y
#traeme la clase que en este caso se llama Clase_Funciones
from Funciones import Funciones_Globales

t=1
class ActionsMouse(unittest.TestCase):
    

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=
        r"C:/Users/alan8/Documents/chromedriver.exe")
        

    def test_Actions_mouse(self):
        driver = self.driver
        driver.implicitly_wait(15)
        driver.maximize_window()
        driver.get("https://demoqa.com/text-box")
        functions = Funciones_Globales(driver)
        functions.texto_mixto("id", "userName", "Admin",)
        functions.texto_mixto("id", "userEmail", "admin@gmail.com")
        functions.Click_mixto("id", "submit")

    def test_double_click(self):
        driver = self.driver
        driver.implicitly_wait(15)
        driver.maximize_window()
        driver.get("https://demoqa.com/buttons")
        functions = Funciones_Globales(driver)
        functions.Mouse_Doble("id", "doubleClickBtn")

    def tearDown(self):
            self.driver.implicitly_wait(5)
            self.driver.close()

if __name__ == "__main__":
	unittest.main(verbosity = 2)