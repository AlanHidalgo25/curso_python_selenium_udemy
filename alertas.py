import unittest
from pyunitreport import HTMLTestRunner
from selenium import webdriver
#para poder elegir varias opciones en un dropdown se importa
from selenium.webdriver.support.ui import Select
from time import sleep

class CompareProducts(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=
        r"C:/Users/alan8/Documents/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(30)
        driver.maximize_window()
        driver.get('http://demo-store.seleniumacademy.com')

    def test_compare_products(self):
        driver = self.driver
        search = driver.find_element_by_id('search')
        search.clear()
        search.send_keys('tee')
        sleep(2)
        #enviar información escrita en el input o formulario
        search.submit()
        
        driver.find_element_by_class_name('link-compare').click()
        sleep(2)
        driver.find_element_by_link_text('Clear All').click()
        sleep(2)

        #cambiar la atención del navegador en la alerta que aparecera
        alert = driver.switch_to.alert
        #trae el texto de la alerta y procedemos a comparar
        alert_text = alert.text

        self.assertEqual('Are you sure you would like to remove all products from your comparison?', alert_text)
        sleep(2)
        
        #hara click en aceptar en la ventana de alerta
        alert.accept()

    def tearDown(self):
        self.driver.implicitly_wait(5)
        self.driver.close()

if __name__ == "__main__":
	unittest.main(verbosity = 2)