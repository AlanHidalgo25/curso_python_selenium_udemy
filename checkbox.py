from typing import KeysView
import unittest
from selenium import webdriver
from time import sleep

class CheckBox(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=
        r"C:/Users/alan8/Documents/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(15)
        driver.maximize_window()
        driver.get('https://demoqa.com/checkbox')

    def test_check_box(self):
        driver = self.driver
        item_home = driver.find_element_by_xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/ol[1]/li[1]/span[1]/button[1]/*[1]")
        item_home.click()
        sleep(1)

        item_desktop = driver.find_element_by_xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/ol[1]/li[1]/ol[1]/li[1]/span[1]/button[1]/*[1]")
        item_desktop.click()
        sleep(1)

        check_notes = driver.find_element_by_xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/ol[1]/li[1]/ol[1]/li[1]/ol[1]/li[1]/span[1]/label[1]/span[1]/*[1]")
        check_notes.click()

    def tearDown(self):
            self.driver.implicitly_wait(5)
            self.driver.close()

if __name__ == "__main__":
	unittest.main(verbosity = 2)