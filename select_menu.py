from typing import KeysView
import unittest
from selenium import webdriver
from time import sleep
from selenium.webdriver.support import select
from selenium.webdriver.support.ui import Select

class SelectMenu(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=
        r"C:/Users/alan8/Documents/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(15)
        driver.maximize_window()
        driver.get('https://demo.seleniumeasy.com/basic-select-dropdown-demo.html')

    def test_select_menu(self):
        driver = self.driver
        acti_options = []
        select_day = Select(driver.find_element_by_id("select-demo"))
        select_country = Select(driver.find_element_by_id("multi-select"))

        for option in select_day.options:
            acti_options.append(option.text)
        
        print(acti_options)

        select_day.select_by_index(3)
        sleep(1)
        select_country.select_by_index(3)
        sleep(2)


    def tearDown(self):
            self.driver.implicitly_wait(5)
            self.driver.close()

if __name__ == "__main__":
	unittest.main(verbosity = 2)