from typing import KeysView
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
import random

class FormValidation(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=
        r"C:/Users/alan8/Documents/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(15)
        driver.maximize_window()
        driver.get('https://demo.seleniumeasy.com/input-form-demo.html')

    def create_phone(self): 
         # Segundo dígito 
        second = [3, 4, 5, 7, 8][random.randint(0, 4)] 
         # Tercer dígito 
        third = { 3: random.randint(0, 9), 
        4: [5, 7, 9][random.randint(0, 2)], 
        5: [i for i in range(10) if i != 4][random.randint(0, 8)], 
        7: [i for i in range(10) if i not in [4, 9]][random.randint(0, 7)], 
        8: random.randint(0, 9), }[second] 
         # Últimos ocho dígitos 
        suffix = random.randint(999999,1000000) 
         # Fusionar número de teléfono 
        return "1{}{}{}".format(second, third, suffix) 
         # Generar número de teléfono móvil 
    #phone = create_phone()
    #print(phone)


    def test_form_validation(self):
        driver = self.driver

        try:
            first_name = WebDriverWait(driver,5).until(EC.visibility_of_element_located((By.NAME, "first_name")))
            email = WebDriverWait(driver,5).until(EC.visibility_of_element_located((By.NAME, "email")))
            phone = WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.NAME, "phone")))

            first_name = driver.find_element_by_name("first_name")
            first_name.send_keys("test 1")
            last_name = driver.find_element_by_name("last_name")
            last_name.send_keys("QA")
            email = driver.find_element_by_name("email")
            email.send_keys("test1@gmail.com")
            phone = driver.find_element_by_name("phone")
            phone.send_keys(self.create_phone())
            address = driver.find_element_by_name("address")
            address.send_keys("Cancun alla te vamos quieras o no")
            city = driver.find_element_by_name("city")
            city.send_keys("Acapulco")
            zip_code = driver.find_element_by_name("zip")
            zip_code.send_keys("1220")
            website = driver.find_element_by_name("website")
            website.send_keys("google.com")
            hosting = driver.find_elements_by_xpath('input[type="radio"][value="no"]')
            project_description = driver.find_element_by_name("comment")
            project_description.send_keys("hola mundo")

            select_state = Select(driver.find_element_by_name("state"))
            select_state.select_by_visible_text("New York")
            sleep(20)
        except Exception as e:
            print(e)
            print("No hizo el ingreso de data al formulario")


    def tearDown(self):
            self.driver.implicitly_wait(5)
            self.driver.close()

if __name__ == "__main__":
	unittest.main(verbosity = 2)