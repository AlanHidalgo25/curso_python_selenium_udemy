from typing import KeysView
import unittest
from selenium import webdriver
from time import sleep

class HolaMundo(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=
        r"C:/Users/alan8/Documents/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(15)
        driver.maximize_window()
        driver.get('https://www.google.com/')

    def test_hello_world(self):
        driver = self.driver
        search = driver.find_element_by_name('q')
        search.click()
        search.send_keys('Hoooooooola')
        sleep(2)
        print('hola mundo, curso nuevo desde udemy')

    def tearDown(self):
        self.driver.implicitly_wait(5)
        self.driver.close()

if __name__ == "__main__":
	unittest.main(verbosity = 2)