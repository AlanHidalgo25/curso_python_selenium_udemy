import unittest
from pyunitreport import HTMLTestRunner
from selenium import webdriver
#para poder elegir varias opciones en un dropdown se importa
from selenium.webdriver.support.ui import Select
from time import sleep
#importamos de la siguiente manera
#de la carpeta Funciones selecciona el archivo que se llama Funciones y
#traeme la clase que en este caso se llama Clase_Funciones
from Funciones import Funciones_Globales


class Base_Test(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=
        r"C:/Users/alan8/Documents/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()
        driver.get('https://demo.seleniumeasy.com/input-form-demo.html')

    def test1(self):
        driver = self.driver
        f = Funciones_Globales(driver)
        f.saludos()
        f.tiempo(5)
        
    def tearDown(self):
        self.driver.implicitly_wait(5)
        self.driver.close()

if __name__ == "__main__":
	unittest.main(verbosity = 2)