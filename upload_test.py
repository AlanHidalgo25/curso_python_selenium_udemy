from typing import KeysView
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class UploadTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=
        r"C:/Users/alan8/Documents/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(15)
        driver.maximize_window()
        driver.get('https://testpages.herokuapp.com/styled/file-upload-test.html')

    def test_upload(self):
        driver = self.driver
        try:
            #Se espera que el boton este presente y se busca por el ID
            upload_file = WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.ID, "fileinput")))
            upload_file = driver.find_element_by_id("fileinput")
            type_image = driver.find_element_by_id("itsanimage")
            type_image.click()
            sleep(1)

            upload_file.send_keys("C:/Users/alan8\Documents/Udemy_Python_Selenium/images/descarga.jpg")
            sleep(1)
            upload_button = driver.find_element_by_name("upload")
            upload_button.click()

            sleep(1)


        except Exception as e:
            print(e)
            print("El elemento no esta disponible")


    def tearDown(self):
            self.driver.implicitly_wait(5)
            self.driver.close()

if __name__ == "__main__":
	unittest.main(verbosity = 2)