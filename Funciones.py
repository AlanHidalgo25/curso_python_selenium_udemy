import unittest
from pyunitreport import HTMLTestRunner
from selenium import webdriver
#para poder elegir varias opciones en un dropdown se importa
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from time import sleep
import time
import random
from selenium.webdriver import ActionChains

class Funciones_Globales():
    
    def __init__(self,driver):
        self.driver = driver

    def saludos(self):
        print("Saludos desde la clase funciones globales, POM")

    def tiempo(self,wait):
        t=time.sleep(wait)
        return t

    #esta función permite llamar a cualquier id, name, xpath
    #solo hay que llamar el metodo,ver ejemplo archivo test_excel
    def texto_mixto(self, tipo, selector, texto, tiempo=1):
        if(tipo == "xpath"):
            try:
                var = WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, selector)))
                var = self.driver.find_element_by_xpath(selector)
                var.clear()
                var.send_keys(texto)
                t = time.sleep(tiempo)
                return t
            except Exception as e:
                print("No se encontro el elemento" + selector)
                print(e)
        elif(tipo == "id"):
            try:
                var = WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.ID, selector)))
                var = self.driver.find_element_by_id(selector)
                var.clear()
                var.send_keys(texto)
                t = time.sleep(tiempo)
                return t
            except Exception as e:
                print("No se encontro el elemento" + selector)
                print(e)
        elif(tipo == "name"):
            try:
                var = WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.NAME, selector)))
                var = self.driver.find_element_by_name(selector)
                var.clear()
                var.send_keys(texto)
                t = time.sleep(tiempo)
                return t
            except Exception as e:
                print("No se encontro el elemento" + selector)
                print(e)

    #esta función permite hacer click a cualquier elemento de la pagina
    #solo hay que llamarla y listo
    def Click_mixto(self, tipo, selector, tiempo=.2):
            if(tipo == "xpath"):
                try:
                    var = WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, selector)))
                    var = self.driver.find_element_by_xpath(selector)
                    var.click()
                    t = time.sleep(tiempo)
                    return t
                except Exception as e:
                    print("No se encontro el elemento" + selector)
                    print(e)
            elif(tipo == "id"):
                try:
                    var = WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.ID, selector)))
                    var = self.driver.find_element_by_id(selector)
                    var.click()
                    t = time.sleep(tiempo)
                    return t
                except Exception as e:
                    print("No se encontro el elemento" + selector)
                    print(e)
            elif(tipo == "name"):
                try:
                    var = WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.NAME, selector)))
                    var = self.driver.find_element_by_name(selector)
                    var.click()
                    t = time.sleep(tiempo)
                    return t
                except Exception as e:
                    print("No se encontro el elemento" + selector)
                    print(e)

    def Mouse_Derecho(self,tipo,selector,tiempo=1):
        if(tipo=="xpath"):
            try:
                val=self.SEX(selector)
                act = ActionChains(self.driver)
                act.context_click(val).perform()
                print("ClickDerecho en {}".format(selector))
                t = time.sleep(tiempo)
                return t
            except Exception as e:
                print("No se encontro el elemento" + selector)
                print(e)

        elif(tipo == "id"):
            try:
                val=self.SEI(selector)
                act = ActionChains(self.driver)
                act.context_click(val).perform()
                #print("ClickDerecho en {}".format(selector))
                t = time.sleep(tiempo)
              
            except Exception as e:
                print("No se encontro el elemento" + selector)
                print(e)

    def Mouse_Doble(self,tipo,selector,tiempo=1):
        if(tipo=="xpath"):
            try:
                val = WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, selector)))
                val = self.driver.find_element_by_xpath(selector)
                act = ActionChains(self.driver)
                act.double_click(val).perform()
                #print("DoubleClick en {}".format(selector))
                t = time.sleep(tiempo)
                return t
            except Exception as ex:
                print(ex.msg)
                print("No se encontro el Elemento" + selector)
                
        elif(tipo == "id"):
            try:
                val = WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.ID, selector)))
                val = self.driver.find_element_by_id(selector)
                act = ActionChains(self.driver)
                act.double_click(val).perform()
                #print("DoubleClick en {}".format(selector))
                t = time.sleep(tiempo)
                return t
            except Exception as e:
                print("No se encontro el elemento" + selector)
                print(e)

    def create_phone(self): 
         # Segundo dígito 
        second = [3, 4, 5, 7, 8][random.randint(0, 4)] 
         # Tercer dígito 
        third = { 3: random.randint(0, 9), 
        4: [5, 7, 9][random.randint(0, 2)], 
        5: [i for i in range(10) if i != 4][random.randint(0, 8)], 
        7: [i for i in range(10) if i not in [4, 9]][random.randint(0, 7)], 
        8: random.randint(0, 9), }[second] 
         # Últimos ocho dígitos 
        suffix = random.randint(999999,1000000) 
         # Fusionar número de teléfono 
        return "1{}{}{}".format(second, third, suffix) 
         # Generar número de teléfono móvil 
    #phone = create_phone()
    #print(phone)
                
            