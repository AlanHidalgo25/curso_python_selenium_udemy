import unittest
from pyunitreport import HTMLTestRunner
from selenium import webdriver
#para poder elegir varias opciones en un dropdown se importa
from selenium.webdriver.support.ui import Select
from time import sleep
#importamos de la siguiente manera
#de la carpeta Funciones selecciona el archivo que se llama Funciones y
#traeme la clase que en este caso se llama Clase_Funciones
from Funciones import Funciones_Globales
from Funciones_Excel import *

t=1
class Base_Test(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=
        r"C:/Users/alan8/Documents/chromedriver.exe")
        driver = self.driver
        driver.implicitly_wait(20)
        driver.maximize_window()
        driver.get('https://demoqa.com/text-box')

    def test1(self):
        driver = self.driver
        f = Funciones_Globales(driver)
        function_excel = Funexcel(driver)
        f.saludos()
        ruta_excel = "C:/Users/alan8/Documents/Data_Prueba.xlsx"
        filas = function_excel.getRowCount(ruta_excel, "Hoja1")
        #print(filas)

        for i in range(2, filas+1):
            try:
                nombre=function_excel.readData(ruta_excel, "Hoja1", i, 1)
                email=function_excel.readData(ruta_excel, "Hoja1", i, 2)
                dir1=function_excel.readData(ruta_excel, "Hoja1", i, 3)
                dir2=function_excel.readData(ruta_excel, "Hoja1", i, 4)

                f.texto_mixto("xpath", "//input[@id='userName']", nombre, t)
                f.texto_mixto("id", "userEmail", email, t)
                f.texto_mixto("id", "currentAddress", dir1, t)
                f.texto_mixto("id", "permanentAddress", dir2, t)
                f.Click_mixto("id", "submit", t)
                function_excel.writeData(ruta_excel,"Hoja1", i, 5, "Dato Insertado")

            except Exception as e:
                print(e)

        
    def tearDown(self):
        self.driver.implicitly_wait(5)
        self.driver.close()

if __name__ == "__main__":
	unittest.main(verbosity = 2)